var gulp                                                      = require('gulp');


var  htmlmin                                          = require('gulp-htmlmin');
var gulpif                                                 = require('gulp-if');

var concat                                             = require('gulp-concat');
var VIEW_T_IN_SERVER_PART                       = '03_ViewsT/aa_WorkSpace/**/*.*';
var VIEW_OUT_SERVER_PART                        = 'aa_template/resources/views';
 



module.exports =
{
            
            
             html5_minify_view: function ()
            {
               
                var options_view = {
                    VIEW_T_IN_SERVER_PART: VIEW_T_IN_SERVER_PART,
                    VIEW_OUT_SERVER_PART: VIEW_OUT_SERVER_PART 
                };
                
                VIEW_MOVE_HTML(options_view);

            } 

           


 
 };
 
 
 global.BODY_HTML = function (options)
{
               return gulp.src( options.VIEW_T_IN_FRAG )
                .pipe(concat('body.blade.php'))
                .pipe(gulp.dest(options.VIEW_T_OUT_BODY));

};
 
 global.VIEW_MOVE_HTML = function (options)
{
                 return   gulp.src(options.VIEW_T_IN_SERVER_PART)
               .pipe(gulpif(process.env.NODE_ENV === 'production',htmlmin({
                    collapseWhitespace: true,
                    removeAttributeQuotes: true,
                    removeComments: true,
                    minifyJS: true,
                    //ignoreCustomFragments: [/@for[\s\S]*@endfor/, /@if[\s\S]*@endif/, /@php[\s\S]*@endphp/,/"{!!.*!!}"/,]
                }) ))

                .pipe(gulp.dest(options.VIEW_OUT_SERVER_PART));

};
 
 

  