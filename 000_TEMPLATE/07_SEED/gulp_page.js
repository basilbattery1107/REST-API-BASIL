var SCRIPTS_OUT_PATH                                                = 'public/101_SCRIPTS';
var SCRIPTS_IN_PATH                                    = '101_SCRIPTS/<%= DNAME %>/**/*.*';
var SCRIPTS_OUT_FILE_NAME                                               = '<%= NAME %>.js';


var SASS_OUT_PATH                                                       = 'public/100_CSS';
var SASS_IN_PATH                                      = '100_SASS/<%= DNAME %>/style.scss';
var SASS_OUT_FILE_NAME                                                 = '<%= NAME %>.css';

var VIEW_T_IN_FRAG                         = '103_ViewsT/ab_Fragments/<%= DNAME %>/**/*.*';
var VIEW_T_OUT_BODY                      = '103_ViewsT/aa_ServerPart/ab_body/<%= DNAME %>';
 

module.exports = 
{
  
  <%= NAME %>_mode: function () 
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  }
  
  
  
};