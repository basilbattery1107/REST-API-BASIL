var gulp                                                      = require('gulp');
var util                                                 = require('gulp-util');
global.page_template = require('./zz_tasks/zz_COMMON_TASKS/d_make_template.js');

var                                     gulpSequence = require('gulp-sequence');
global.task_pages_sass_js_body=[];









if(util.env.name !== undefined)
{


    gulp.task('make-page', function()
    {

        return  page_template.make_template();


    });


    gulp.task('del-page', function()
    {

        return  page_template.remove_template();


    });

    gulp.task('make-db', function()
    {

        return  page_template.make_db();


    });

    gulp.task('del-db', function()
    {

        return  page_template.delete_db();


    });


}
else
{
    global.config                                          = require('./config.js');
    //Directory Structire
    var requireDir       = require('require-dir');
    var common_tasks     = requireDir('./zz_tasks/zz_COMMON_TASKS');
    var env_tasks        = requireDir('./zz_tasks/zz_ENV_VARIABLES');
    var webpage_tasks    = requireDir('./zz_tasks/aa_WEBPAGES');
    var core_tasks       = requireDir('./zz_tasks');




    const                                      fs = require('fs');
    const                  testFolder = './zz_tasks/aa_WEBPAGES/';

    global.browserSync = require('browser-sync').create();
    process.env.TEST_PAGE  = config.pagelink;
    //console.log(config.pagelink+"oooooo");




//Common tasks
    gulp.task('clear'             , common_tasks.b_miscellaneous.clear);


    gulp.task('browser-init'      , common_tasks.a_browser.browser_init);
    gulp.task('browser-reload'    , common_tasks.a_browser.browser_reload);
    gulp.task('javaScriptError'   , core_tasks.a_javascript.javaScriptError);


///Environment variables
    gulp.task('set-mode-dev-env'    , env_tasks.common_env.set_mode_dev_env);
    gulp.task('set-mode-deploy-env' , env_tasks.common_env.set_mode_deploy_env);



    // miscellaneous
    gulp.task('clean-without-image' , common_tasks.c_clean.clean_without_image);
    gulp.task('clean-with-image'    , common_tasks.c_clean.clean_with_image);

//Common specific
    gulp.task('javascript-move'     , core_tasks.a_javascript.javascript_move);
    gulp.task('sass-move'           , core_tasks.b_sass.sass_move);
    gulp.task('html5-body-move'     , core_tasks.c_html.html5_body_move);
    gulp.task('html5-minify-view'   , core_tasks.c_html.html5_minify_view);
    gulp.task('image-move'          , core_tasks.d_images.image_move);












    gulp.task('init', function()
    {



        var i;
        for (i = 0; i < config.items.length; i++) {

            if(config.items[i][1])
            {

                global.fvv = require('./zz_tasks/aa_WEBPAGES/'+config.items[i][0] +'.js');

                gulp.task(config.items[i][0] ,fvv.get_function() );
            }


        }




        task_pages_sass_js_body[0]='javaScriptError';

        for (i = 0,j=1; i <config.items.length; i++) {

            if(config.items[i][1]==1)
            {
                task_pages_sass_js_body[j++]=config.items[i][0] ;

            }


        }

    });


    gulp.task('pages',function (cb)
    {
               //console.log("&&&&&&&]n"+task_pages_sass_js_body);
               gulpSequence(task_pages_sass_js_body,'html5-minify-view')(cb);
    });

    gulp.task('depF',function (cb)
    {
        gulpSequence(
            'clear',
            'clean-without-image',
            'init',
            'set-mode-deploy-env',
            'pages'
            )(function (cb) {

            var execSync = require('child_process').execSync;
            var route ='npm run production';
            execSync(route,{
                cwd:'aa_template'
            });

            console.log("Done");
        });




    });




    gulp.task('watch',function (cb)
    {
        var SASS_CHECK_PATH                                          = '01_SASS/**/*.*';
        var SCRIPTS_CHECK_PATH                                    = '02_SCRIPTS/**/*.*';

        var PHP_PATH_BOOT                                = 'aa_template/routes/web.php';
        var PHP_PATH_SRC                              = 'aa_template/app/Http/Controllers/**/*.php';
        var VIEW_T_WORK_SPACE              = '03_ViewsT/aa_WorkSpace/**/*.*';
        var VIEW_T_WORK_SPACE_E              = '!03_ViewsT/aa_WorkSpace/zz_body/**/*.*';
        var VIEW_PATH_FRAGMENT                           = '03_ViewsT/ab_Fragments/**/*.*';

        gulp.watch([PHP_PATH_BOOT,PHP_PATH_SRC], function()
        {

           //  gulp.start('browser-reload');
             gulpSequence('browser-reload')(null);






        });


        gulp.watch([SASS_CHECK_PATH,SCRIPTS_CHECK_PATH,VIEW_T_WORK_SPACE,VIEW_T_WORK_SPACE_E,VIEW_PATH_FRAGMENT], function ()
        {



           // gulp.start('pages-watch');

           // gulpSequence('pages-watch')(null);
            gulpSequence(
                'clear',
                'clean-without-image',
                'init',
                'pages',
                'browser-reload')(null);


        });
    });


    gulp.task('test',function (cb)
    {

/*
        var part_1 = "public  function"+" _"+options.NAME+"()"+"{";
        var part_2 = "return view("+"\""+"aa_ServerPart.aa_WorkSpace"+"."+options.DNAME+".main"+"\""+"); }";


        execSync( 'sed -i \'/)/a '+part_1+part_2+"\'"+'   '+dest,{
            cwd:'aa_template'
        });
        console.log("Done function injection in controller")
*/
    });

    gulp.task('dev',function (cb)
    {



        gulpSequence(
            'clear',
            'clean-without-image',
            'init',
            'set-mode-dev-env',
            'pages',
            'browser-init',
            'watch')(cb);



    });


    gulp.task('dep',function (cb)
    {



        gulpSequence(
            'clear',
            'clean-without-image',
            'init',
            'set-mode-deploy-env',
            'pages',
            'browser-init',
            'watch')(cb);



    });




}




